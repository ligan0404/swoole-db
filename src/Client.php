<?php
/**
 * @Author: gan
 * @Description:
 * @File:  Client
 * @Version: 1.0.0
 * @Date: 2022/1/27 11:54 上午
 */
declare(strict_types=1);

namespace DB;

use DB\Exception\ConnectException;

class Client
{
    private static Client $instance;

    public static function getInstance()
    {
        if (empty(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function get(string $key)
    {
        $retry = 0;
        back:
        $retry++;
        $mysql = Manager::getInstance()->connect($key);
        if ($mysql->connected && $mysql->errno == 0) {
            return $mysql;
        } elseif ($retry <= 3) {
            goto back;
        } else {
            throw new ConnectException('Mysql Client Connect Error');
        }
    }
}
