<?php
/**
 * @Author:     gan
 * @Description:
 * @File:       DB
 * @Version:    1.0.0
 * @Date:       2022/1/27 11:52 上午
 */
declare(strict_types=1);

namespace DB;

use DB\Exception\QueryException;
use Swoole\Coroutine;

class Db
{
    private static Db    $instance;
    private static array $sql     = [];
    private static array $mysql   = [];
    private static array $channel = [];


    /**
     * @var string
     */
    private string $connectName = "";

    public static function getInstance()
    {
        if (empty(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function init(string $channel = 'client')
    {
        self::$channel[Coroutine::getCid()] = $channel;
        return $this;
    }

    /**
     * 设置表名
     * @param string $table 表名
     * @return $this
     */
    public function table(string $table)
    {
        $this->addParam('table', trim($table));
        return $this;
    }

    /**
     * 设置字段
     * @param string $field 字段
     * @return $this
     */
    public function field(string $field)
    {
        $this->addParam('field', trim($field));
        return $this;
    }

    /**
     * 设置join条件
     * @param string $table 表名
     * @param string $condition 条件
     * @param string $lr 作用域 LEFT,RIGHT,''
     * @return $this
     */
    public function join(string $table, string $condition, string $lr = '')
    {
        switch (trim($lr)) {
            case 'left':
                $direction = 'LEFT JOIN';
                break;
            case 'right':
                $direction = 'RIGHT JOIN';
                break;
            default:
                $direction = 'JOIN';
        }
        $this->addParam('join', $direction . ' ' . trim($table) . ' ON ' . trim($condition), 2);
        return $this;
    }

    /**
     * 设置group条件
     * @param string $field 字段
     * @return $this
     */
    public function group(string $field)
    {
        $this->addParam('group', trim($field), 2);
        return $this;
    }

    /**
     * 设置having条件
     * @param string $field 字段
     * @param string $operator 条件
     * @param string $value 值
     * @return $this
     */
    public function having(string $field, string $operator, string $value)
    {
        $this->addParam('having', trim($field) . ' ' . trim($operator) . ' ' . trim($value), 2);
        return $this;
    }

    /**
     * 设置order条件
     * @param string $field 字段
     * @param string $sort 排序 ASC DESC
     * @return $this
     */
    public function order(string $field, string $sort)
    {
        $this->addParam('order', trim($field) . ' ' . strtoupper(trim($sort)), 2);
        return $this;
    }


    public function limit(...$argv)
    {
        $this->addParam('limit', implode(',', $argv));
        return $this;
    }

    /**
     * 设置where AND
     * @param mixed ...$argv 变长参，2位则表示=
     * @return $this
     * @throws QueryException
     */
    public function where(...$argv)
    {
        switch (count($argv)) {
            case 2:
                $this->addParam('where', [$this->buildWhere($argv[0], '=', $argv[1]), 'AND'], 2);
                break;
            case 3:
                $this->addParam('where', [$this->buildWhere($argv[0], $argv[1], $argv[2]), 'AND'], 2);
                break;
            default:
                throw new QueryException('Where Param Error');
        }
        return $this;
    }

    /**
     * 设置where OR
     * @param mixed ...$argv | 变长参，2位则表示=
     * @return $this
     * @throws QueryException
     */
    public function whereOr(...$argv)
    {
        switch (count($argv)) {
            case 2:
                $this->addParam('where', [$this->buildWhere($argv[0], '=', $argv[1]), 'OR'], 2);
                break;
            case 3:
                $this->addParam('where', [$this->buildWhere($argv[0], $argv[1], $argv[2]), 'OR'], 2);
                break;
            default:
                throw new QueryException('whereOr Param Error');
        }
        return $this;
    }

    /**
     * 执行sql语句[预处理]
     * @param string $sql sql语句
     * @param array $p 预处理参数
     * @return mixed
     * @throws QueryException|Exception\ConnectException
     */
    public function query(string $sql, array $p = [])
    {
        return $this->prepare(trim($sql), $p, 1);
    }

    /**
     * 单条查询 | 成功返回1维数组或空数组，失败返回null
     * @param bool $buildSql 是否返回sql
     * @return mixed|string
     * @throws Exception\ConnectException
     * @throws QueryException
     */
    public function find(bool $buildSql = false)
    {
        $res = $this->buildSql('find', $buildSql);
        if ($buildSql === true) {
            return $res;
        } else {
            return !$res ? $res : $res[0];
        }
    }

    /**
     * 多条查询 | 成功返回2维数组或空数组，失败返回null
     * @param bool $buildSql 是否返回sql
     * @return mixed|string
     * @throws Exception\ConnectException
     * @throws QueryException
     */
    public function select(bool $buildSql = false)
    {
        return $this->buildSql('select', $buildSql);
    }

    /**
     * 列查询 | 成功返回1维2维或空数组，失败返回null
     * @param string $field : 查询字段
     * @param string $index 查询索引
     * @param bool $buildSql 是否返回sql
     * @return array|mixed|string
     * @throws Exception\ConnectException
     * @throws QueryException
     */
    public function column(string $field, string $index = '', bool $buildSql = false)
    {
        $_field = $index != '' ? trim($field) . ',' . trim($index) : trim($field);
        $this->addParam('field', $_field);
        $res = $this->buildSql('column', $buildSql);
        if ($buildSql === true) {
            return $res;
        } else {
            if ($res) {
                switch (count(explode(',', $_field))) {
                    case 1:
                        $res = array_column($res, $field);
                        break;
                    case 2:
                        $res = $index != '' ? array_column($res, $field, $index) : $res;
                        break;
                    default:
                        $res = $index != '' ? array_column($res, null, $index) : $res;
                }
            }
            return $res;
        }
    }

    /**
     * 获取某的字段的值 | 成功返回字段值，失败返回false
     * @param string $field | 查询字段
     * @param bool $buildSql 是否返回sql
     * @return bool|mixed|string
     * @throws Exception\ConnectException
     * @throws QueryException
     */
    public function value(string $field, bool $buildSql = false)
    {
        $this->addParam('field', $field);
        $res = $this->buildSql('value', $buildSql);
        if ($buildSql === true) {
            return $res;
        } else {
            return isset($res[0][$field]) ? $res[0][$field] : null;
        }
    }

    /**
     * 聚合查询count | 成功返回总数，失败返回null
     * @param string $field | 查询字段
     * @param bool $buildSql 是否返回sql
     * @return mixed|string
     * @throws Exception\ConnectException
     * @throws QueryException
     */
    public function count(string $field = '*', bool $buildSql = false)
    {
        $this->addParam('field', "COUNT({$field}) `count`");
        $res = $this->buildSql('count', $buildSql);
        if ($buildSql === true) {
            return $res;
        } else {
            return !$res ? $res : $res[0]['count'];
        }
    }

    /**
     * 聚合查询sum | 成功返回总数，失败返回null
     * @param string $field | 查询字段
     * @param bool $buildSql 是否返回sql
     * @return mixed|string
     * @throws Exception\ConnectException
     * @throws QueryException
     */
    public function sum(string $field, bool $buildSql = false)
    {
        $this->addParam('field', "SUM({$field}) `sum`");
        $res = $this->buildSql('sum', $buildSql);
        if ($buildSql === true) {
            return $res;
        } else {
            return !$res ? $res : $res[0]['sum'];
        }
    }

    /**
     * 聚合查询
     * @param string $field
     * @param bool $buildSql
     * @return mixed|string
     * @throws Exception\ConnectException
     * @throws QueryException
     */
    public function max(string $field, bool $buildSql = false)
    {
        $this->addParam('field', "MAX({$field}) `max`");
        $res = $this->buildSql('max', $buildSql);
        if ($buildSql === true) {
            return $res;
        } else {
            return !$res ? $res : $res[0]['max'];
        }
    }

    /**
     * 聚合查询
     * @param string $field
     * @param bool $buildSql
     * @return mixed|string
     * @throws QueryException|Exception\ConnectException
     */
    public function min(string $field, bool $buildSql = false)
    {
        $this->addParam('field', "MIN({$field}) `min`");
        $res = $this->buildSql('min', $buildSql);
        if ($buildSql === true) {
            return $res;
        } else {
            return !$res ? $res : $res[0]['min'];
        }
    }

    /**
     * 聚合查询
     * @param string $field
     * @param bool $buildSql
     * @return mixed|string
     * @throws Exception\ConnectException
     * @throws QueryException
     */
    public function avg(string $field, bool $buildSql = false)
    {
        $this->addParam('field', "AVG({$field}) `avg`");
        $res = $this->buildSql('avg', $buildSql);
        if ($buildSql === true) {
            return $res;
        } else {
            return !$res ? $res : $res[0]['avg'];
        }
    }

    /**
     * 分页查询 | 成功返回2维数组或空数组，失败返回null
     * @param int $page | 页码
     * @param int $limit | 单页条数
     * @param bool $buildSql 是否返回sql
     * @return mixed|string
     * @throws QueryException|Exception\ConnectException
     */
    public function paginate(int $page = 1, int $limit = 10, bool $buildSql = false)
    {
        $this->addParam('limit', ($page - 1) * $limit . "," . $limit);
        return $this->buildSql('paginate', $buildSql);
    }

    /**
     * 单条写入 | 成功返回主键ID，失败返回false
     * @param array $data | 一维数组
     * @param bool $buildSql 是否返回sql
     * @return mixed|string
     * @throws QueryException|Exception\ConnectException
     */
    public function insert(array $data, bool $buildSql = false)
    {
        $this->addParam('field', '(' . implode(',', array_map(function ($v) {
            return "`{$v}`";
        }, array_keys($data))) . ')');
        $this->addParam('values', '(' . $this->buildAsk(count($data)) . ')', 2);
        $this->addStmt($data);
        return $this->buildSql('insert', $buildSql);
    }

    /**
     * 批量写入 | 成功返回插入条数，失败返回false
     * @param array $data | 二维数组
     * @param bool $buildSql 是否返回sql
     * @return mixed|string
     * @throws QueryException|Exception\ConnectException
     */
    public function insertAll(array $data, bool $buildSql = false)
    {
        $_data = end($data);
        $ask   = $this->buildAsk(count($_data));
        $this->addParam('field', '(' . implode(',', array_map(function ($v) {
            return "`{$v}`";
        }, array_keys($_data))) . ')');
        foreach ($data as  $v) {
            $this->addStmt($v);
            $this->addParam('values', "({$ask})", 2);
        }
        return $this->buildSql('insertAll', $buildSql);
    }

    /**
     * 单条更新 | 成功返回更新的条数，失败返回false
     * @param array $data | 一维数组
     * @param bool $buildSql 是否返回sql
     * @return mixed|string
     * @throws QueryException|Exception\ConnectException
     */
    public function update(array $data, bool $buildSql = false)
    {
        foreach ($data as $k => $v) {
            if (is_array($v)) {
                $this->addParam('set', "`{$k}`=`{$v[0]}`{$v[1]}{$v[2]}", 2);
            } else {
                $this->addParam('set', "`{$k}`" . '=' . $this->buildType($v), 2);
            }
        }
        return $this->buildSql('update', $buildSql);
    }

    /**
     * 批量更新 | 成功返回更新的条数，失败返回false
     * @param array $data | 二维数组
     * @param bool $buildSql 是否返回sql
     * @return float|int|mixed|string
     * @throws QueryException|Exception\ConnectException
     */
    public function updateAll(array $data, bool $buildSql = false)
    {
        $_data = end($data);
        $ask   = $this->buildAsk(count($_data));
        $this->addParam('field', '(' . implode(',', array_map(function ($v) {
            return "`{$v}`";
        }, array_keys($_data))) . ')');
        foreach ($_data as $k => $v) {
            if (is_array($v)) {
                $this->addParam('duplicate', "`{$k}`=VALUES(`{$v[0]}`){$v[1]}`{$k}`", 2);
            } else {
                $this->addParam('duplicate', "`{$k}`=VALUES(`{$k}`)", 2);
            }
        }
        foreach ($data as $k => $v) {
            $this->addParam('values', '(' . $ask . ')', 2);
            $this->addStmt($v);
        }
        return $buildSql === true ? $this->buildSql('updateAll', $buildSql) : $this->buildSql('updateAll', $buildSql) / 2;
    }

    /**
     * 字段递增 | 成功返回true，失败返回false
     * @param $field
     * @param mixed $incr
     * @param bool $buildSql 是否返回sql
     * @return mixed|string
     * @throws QueryException|Exception\ConnectException
     */
    public function setInc(string $field, $incr = 1, bool $buildSql = false)
    {
        $this->addParam('set', "`{$field}`=`{$field}`+{$incr}", 2);
        return $this->buildSql('setInc', $buildSql);
    }

    /**
     * 字段递减 | 成功返回true，失败返回false
     * @param string $field
     * @param mixed $decr
     * @param bool $buildSql 是否返回sql
     * @return mixed|string
     * @throws Exception\ConnectException
     * @throws QueryException
     */
    public function setDec(string $field, $decr = 1, bool $buildSql = false)
    {
        $this->addParam('set', "`{$field}`=`{$field}`-{$decr}", 2);
        return $this->buildSql('setDec', $buildSql);
    }

    /**
     * 删除数据 | 成功返回true，失败返回false
     * @param bool $buildSql 是否返回sql
     * @return mixed|string
     * @throws Exception\ConnectException
     * @throws QueryException
     */
    public function delete(bool $buildSql = false)
    {
        return $this->buildSql('delete', $buildSql);
    }

    /**
     * 开启事务 | 断线重连，开启失败抛出异常，开启成功会阻塞自动回收
     */
    public function begin()
    {
        $retry = 0;
        back:
        $retry++;
        $mysql = ($retry === 1) ? $this->getMysql() : $this->getMysql(true);
        if (!$mysql->begin()) {
            if ($retry < 2) {
                goto back;
            } else {
                throw new QueryException('Mysql Begin Error');
            }
        }
    }

    /**
     * 提交事务 | 成功返回true并回收，失败返回false
     * @return mixed
     * @throws Exception\ConnectException
     */
    public function commit()
    {
        return $this->getMysql()->commit();
    }

    /**
     * 事务回滚 | 成功返回true并回收，失败返回false
     * @return mixed
     * @throws Exception\ConnectException
     */
    public function rollback()
    {
        return $this->getMysql()->rollback();
    }

    public function connect(string $key)
    {
        $this->connectName = $key;
        return $this;
    }


    /**
     * 建立sql语句并查询
     * @param $method
     * @param bool $backSql
     * @return mixed|string
     * @throws QueryException|Exception\ConnectException
     */
    private function buildSql(string $method, bool $backSql = false)
    {
        $cid   = Coroutine::getCid();
        $table = self::$sql[$cid]['table'];
        $field = empty(self::$sql[$cid]['field']) ? '*' : self::$sql[$cid]['field'];
        $join  = empty(self::$sql[$cid]['join']) ? '' : implode(' ', self::$sql[$cid]['join']);
        if (!empty(self::$sql[$cid]['where'])) {
            $where      = 'WHERE ';
            $whereTotal = count(self::$sql[$cid]['where']);
            foreach (self::$sql[$cid]['where'] as $k => $v) {
                if ($k == $whereTotal - 1) {
                    $where .= $v[0];
                } else {
                    $where .= "{$v[0]} {$v[1]} ";
                }
            }
        } else {
            $where = '';
        }
        $group     = empty(self::$sql[$cid]['group']) ? '' : 'GROUP BY ' . implode(',', self::$sql[$cid]['group']);
        $having    = empty(self::$sql[$cid]['having']) ? '' : 'HAVING ' . implode('AND', self::$sql[$cid]['having']);
        $order     = empty(self::$sql[$cid]['order']) ? '' : 'ORDER BY ' . implode(',', self::$sql[$cid]['order']);
        $limit     = empty(self::$sql[$cid]['limit']) ? '' : 'LIMIT ' . self::$sql[$cid]['limit'];
        $values    = empty(self::$sql[$cid]['values']) ? '' : implode(',', self::$sql[$cid]['values']);
        $set       = empty(self::$sql[$cid]['set']) ? '' : implode(',', self::$sql[$cid]['set']);
        $duplicate = empty(self::$sql[$cid]['duplicate']) ? '' : implode(',', self::$sql[$cid]['duplicate']);
        switch ($method) {
            case 'find':
            case 'select':
            case 'column':
            case 'value':
            case 'count':
            case 'sum':
            case 'avg':
            case 'max':
            case 'min':
            case 'paginate':
                $sql  = "SELECT {$field} FROM {$table} {$join} {$where} {$group} {$having} {$order} {$limit}";
                $back = 1;
                break;
            case 'insert':
                $sql  = "INSERT INTO {$table} {$field} VALUE {$values}";
                $back = 2;
                break;
            case 'insertAll':
                $sql  = "INSERT INTO {$table} {$field} VALUES {$values}";
                $back = 3;
                break;
            case 'setInc':
            case 'setDec':
            case 'update':
                $sql  = "UPDATE {$table} SET {$set} {$where}";
                $back = 3;
                break;
            case 'updateAll':
                $sql  = "INSERT INTO {$table} {$field} VALUES {$values} ON DUPLICATE KEY UPDATE {$duplicate}";
                $back = 3;
                break;
            case 'delete':
                $sql  = "DELETE FROM {$table} {$where}";
                $back = 3;
                break;
            default:
                throw new QueryException('Mysql Method Error');
        }
        $sql  = str_replace(['  ', '   '], ' ', trim($sql));
        $stmt = !isset(self::$sql[$cid]['stmt']) ? [] : self::$sql[$cid]['stmt'];
        if (!$backSql) {
            return $this->prepare($sql, $stmt, $back);
        }
        return $sql;
    }

    /**
     * 预处理
     * @param string $sql
     * @param array $stmtArr
     * @param int $backInfo
     * @return bool
     * @throws QueryException|Exception\ConnectException
     */
    private function prepare(string $sql, array $stmtArr, int $backInfo)
    {
        $cid = Coroutine::getCid();
        //初始化sql
        $this->initSql($cid);

        // 如果是读写分离
        if (Manager::$mode == Manager::PROXY) {
            $keyArr   = array_keys(Manager::$dbConf);
            $keyCount = count($keyArr);
            if (strpos($sql, 'SELECT') !== false || strpos($sql, 'select') !== false) {
                $key                            = $keyCount > 1 ? $keyArr[mt_rand(1, $keyCount - 1)] : $keyArr[0];
                self::$sql[$cid]['queryMethod'] = $this->connectName ?: $key;
            } else {
                self::$sql[$cid]['queryMethod'] = $keyArr[0];
            }
        }
        $retry = 0;
        back:
        $retry++;
        $mysql = ($retry === 1) ? $this->getMysql() : $this->getMysql(true);
        $stmt  = $mysql->prepare($sql);
        if ($stmt === false) {
            if ($retry < 2) {
                goto back;
            } else {
                $this->recycle($cid);
                throw new QueryException('Mysql Query Failed');
            }
        } else {
            $this->recycle($cid);
            $result = $stmt->execute($stmtArr);
        }
        if ($result) {
            switch ($backInfo) {
                case 1:
                    $output = $result;
                    break;
                case 2:
                    $output = $mysql->insert_id;
                    break;
                default:
                    $output = $mysql->affected_rows;
            }
        } else {
            $output = false;
        }
        return $output;
    }

    /**
     * 获取连接
     * @param bool $replace 强制重连
     * @return Coroutine\MySQL
     * @throws Exception\ConnectException
     */
    private function getMysql($replace = false): Coroutine\MySQL
    {
        $cid = Coroutine::getCid();
        if (Manager::$mode == Manager::CLUSTER) {
            $keyArr   = array_keys(Manager::$dbConf);
            $keyCount = count($keyArr);
            $key      = $keyCount > 1 ? $keyArr[mt_rand(0, ($keyCount - 1))] : $keyArr[0];
        } elseif (Manager::$mode == Manager::ONE) {
            $key = $this->connectName ?: array_keys(Manager::$dbConf)[0];
        } else {
            $key = self::$sql[$cid]['queryMethod'];
        }
        if ($replace === true) {
            self::$mysql[$cid][$key] = Client::getInstance()->get($key);
        } elseif (!isset(self::$mysql[$cid][$key])) {
            self::$mysql[$cid][$key] = (self::$channel[$cid] == 'client') ? Client::getInstance()->get($key) : Pool::getInstance()->get($key);
        }
        return self::$mysql[$cid][$key];
    }


    /**
     * 资源回收
     * @param $cid
     */
    private function recycle($cid)
    {
        defer(function () use ($cid) {
            if (isset(self::$mysql[$cid])) {
                $keyArr = array_keys(self::$mysql[$cid]);
                if (self::$channel[$cid] == 'pool') {
                    foreach ($keyArr as $key) {
                        Pool::getInstance()->put($key, self::$mysql[$cid][$key]);
                    }
                } else {
                    foreach ($keyArr as $key) {
                        self::$mysql[$cid][$key]->close();
                    }
                }
                unset(self::$mysql[$cid]);
            }
            if (isset(self::$sql[$cid])) {
                unset(self::$sql[$cid]);
            }
            if (isset(self::$channel[$cid])) {
                unset(self::$channel[$cid]);
            }
        });
    }

    /**
     * 初始化查询条件
     * @param $cid :协程ID
     */
    private function initSql($cid)
    {
        self::$sql[$cid] = [
            'queryMethod' => '',
            'table'       => '',
            'field'       => '',
            'limit'       => '',
            'order'       => [],
            'join'        => [],
            'group'       => [],
            'having'      => [],
            'where'       => [],
            'values'      => [],
            'set'         => [],
            'duplicate'   => [],
            'stmt'        => [],
        ];
    }

    /**
     * 构造查询条件
     * @param $key
     * @param $val
     * @param int $mode
     */
    private function addParam($key, $val, $mode = 1)
    {
        if ($mode == 2) {
            self::$sql[Coroutine::getCid()][$key][] = $val;
        } else {
            self::$sql[Coroutine::getCid()][$key] = $val;
        }
    }

    /**
     * 构造预加载值
     * @param $val
     */
    private function addStmt($val)
    {
        $cid = Coroutine::getCid();
        if (is_array($val)) {
            foreach ($val as $v) {
                if (is_array($v)) {
                    self::$sql[$cid]['stmt'][] = $v[2];
                } else {
                    self::$sql[$cid]['stmt'][] = $v;
                }
            }
        } else {
            self::$sql[$cid]['stmt'][] = $val;
        }
    }

    /**
     * 构造where条件
     * @param $field | 字段
     * @param $operator | 条件
     * @param $value | 值
     * @return string
     */
    private function buildWhere($field, $operator, $value)
    {
        $val      = '';
        $operator = strtoupper($operator);
        switch ($operator) {
            case "NOT IN":
            case 'IN':
                if (!is_array($value)) {
                    $value = explode(',', $value);
                }
                $this->addStmt($value);
                $val .= '(';
                $val .= $this->buildAsk(count($value));
                $val .= ')';
                break;
            case 'NOT BETWEEN':
            case 'BETWEEN':
                if (!is_array($value)) {
                    $value = explode(',', $value);
                }
                $this->addStmt($value);
                $val .= "? AND ?";
                break;
            case 'IS NOT NULL':
            case 'IS NULL':
                break;
            default:
                $val .= '?';
                $this->addStmt($value);
        }
        return "{$field} {$operator} {$val}";
    }

    /**
     * 构造预处理问号
     * @param int $total
     * @return string
     */
    private function buildAsk($total)
    {
        $str = '';
        for ($i = 0; $i < $total; $i++) {
            $str .= ($i == 0) ? '?' : ',?';
        }
        return $str;
    }

    /**
     * 判断值类型以确定是否加引号
     * @param $val
     * @return string
     */
    private function buildType($val)
    {
        return is_string($val) ? "'{$val}'" : $val;
    }
}
