<?php
/**
 * @Author: gan
 * @Description:
 * @File:  Manager
 * @Version: 1.0.0
 * @Date: 2022/1/27 11:55 上午
 */
declare(strict_types=1);

namespace DB;

use DB\Exception\ConfigException;
use Swoole\Coroutine\MySQL;

class Manager
{
    private static $instance;
    public static $mode;
    public static $dbConf = [];
    public const   CLUSTER = "cluster";
    public const   PROXY   = "proxy";
    public const   ONE     = "one";

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (empty(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * 注册Mysql策略
     * @param string $mode 集群模式
     * @return Manager
     * @throws ConfigException
     */
    public function regMode(string $mode)
    {
        if (!in_array($mode, [self::ONE, self::CLUSTER, self::PROXY])) {
            throw new ConfigException('Mysql Mode Error');
        } else {
            self::$mode = $mode;
        }
        return $this;
    }

    /**
     * 注册配置
     * @param string $key | 库标志
     * @param array $config | 配置
     * @return $this
     * @throws ConfigException
     */
    public function reg(string $key, array $config)
    {
        //注册mysql连接配置
        if (!isset(self::$dbConf[$key])) {
            if (isset($config['host'], $config['port'], $config['user'], $config['password'], $config['database'], $config['charset'])) {
                self::$dbConf[$key] = $config;
            } else {
                throw new ConfigException('Mysql Db Config Error');
            }
        } else {
            throw new ConfigException('Mysql Register Repeat');
        }
        return $this;
    }

    /**
     * 初始化连接池，仅当配置中有pool配置才会被初始化
     */
    public function initPool()
    {
        $keyArr = array_keys(self::$dbConf);
        foreach ($keyArr as $key) {
            if (isset(self::$dbConf[$key]['pool']['min'], self::$dbConf[$key]['pool']['max'], self::$dbConf[$key]['pool']['retry'])) {
                Pool::getInstance()->init($key);
            }
        }
    }

    /**
     * 连接协程mysql
     * @param $key
     * @return MySQL
     */
    public function connect(string $key)
    {
        $mysql = new MySQL();
        $mysql->connect(self::$dbConf[$key]);
        return $mysql;
    }
}
