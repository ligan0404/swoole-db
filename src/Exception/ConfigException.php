<?php
/**
 * @Author: gan
 * @Description:
 * @File:  ConfigException
 * @Version: 1.0.0
 * @Date: 2022/1/27 1:50 下午
 */
declare(strict_types=1);

namespace DB\Exception;

use Exception;

class ConfigException extends Exception
{
}
