<?php
/**
 * @Author: gan
 * @Description:
 * @File:  QueryException
 * @Version: 1.0.0
 * @Date: 2022/1/27 1:42 下午
 */
declare(strict_types=1);

namespace DB\Exception;

use Exception;

class QueryException extends Exception
{
}
